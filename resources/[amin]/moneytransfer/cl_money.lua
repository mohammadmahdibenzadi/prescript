function LocalPed()
	return GetPlayerPed(-1)
end

--Don't mess with these, changing stuff goes below
local visits = 1
local l = 0
local area = 0
local onjob = false

--these are all the delivery locations, sorted from close to GoPostal to far from it (so earn less for close runs, more for far ones)
local destination = {
{ x = 231.11, y = 215.06, z = 106.14, money = 288},
{ x = 151.94, y = -1036.44, z = 29.34, money = 325},
{ x = -1091.2, y = 2708.25, z = 18.97, money = 415},
{ x = 1965.27, y = 3739.74, z = 32.32, money = 312},
{ x = 29.04, y = -1350.67, z = 29.33, money = 420},
{ x = -54.06, y = -1758.14, z = 29.13, money = 256},
{ x = -2968.25, y = 482.59, z = 15.47, money = 395},
{ x = -113.11, y = 6460.65, z = 31.47, money = 410},
{ x = 1701.22, y = 6424.28, z = 32.64, money = 335},
{ x = 1697.03, y = 4931.03, z = 42.08, money = 369},
{ x = 543.83, y = 2675.15, z = 42.15, money = 418},
{ x = 1175.32, y = 2701.86, z = 38.17, money = 369},
{ x = -3237.73, y = 1004.43, z = 12.45, money = 415},
{ x = 2560.98, y = 385.4, z = 108.62, money = 434},
{ x = -1820.48, y = 786.51, z = 138.02, money = 552},
}

function drawTxt(text, font, centre, x, y, scale, r, g, b, a)
	SetTextFont(font)
	SetTextProportional(0)
	SetTextScale(scale, scale)
	SetTextColour(r, g, b, a)
	SetTextDropShadow(0, 0, 0, 0, 255)
	SetTextEdge(1, 0, 0, 0, 255)
	SetTextDropShadow()
	SetTextOutline()
	SetTextCentre(centre)
	SetTextEntry("STRING")
	AddTextComponentString(text)
	DrawText(x, y)
end

RegisterNetEvent("nomt")
AddEventHandler("nomt", function()
	print('NoDelivery')
	SetNotificationTextEntry("STRING");
	AddTextComponentString("~r~Shoma Maamore Hamle Pool nistid! Be edare kar morajee konid." );
	DrawNotification(false, true);
end)

RegisterNetEvent("yesmt")
AddEventHandler("yesmt", function()
	print('YesDelivery')
    SpawnVan()
	SetNotificationTextEntry("STRING");
	AddTextComponentString("~g~Safar be salamat!" );
	DrawNotification(false, true);
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)
		DrawMarker(1, 42.79, -655.95, 31.63 - 1, 0, 0, 0, 0, 0, 0, 3.0001, 3.0001, 1.5001, 255, 165, 0,165, 0, 0, 0,0) 
		if GetDistanceBetweenCoords(42.79, -655.95, 31.63, GetEntityCoords(LocalPed())) < 2.0 then
			basiccheck()
		end
		if onjob == true then 
			if GetDistanceBetweenCoords(destination[l].x,destination[l].y,destination[l].z, GetEntityCoords(GetPlayerPed(-1))) < 3.0 then
				if IsVehicleModel(GetVehiclePedIsIn(GetPlayerPed(-1), true), GetHashKey("stockade"))  then
					drawTxt('Baraye tahvile baste ~g~E~s~ ra feshar dahid.', 2, 1, 0.5, 0.8, 0.6, 255, 255, 255, 255)
					if (IsControlJustReleased(1, 38)) then
						deliverysucces()
					end
				end
			end
		end
	end
end)


function basiccheck()
	if onjob == false then 
		if (IsInVehicle()) then
			if IsVehicleModel(GetVehiclePedIsIn(GetPlayerPed(-1), true), GetHashKey("stockade")) then
				drawTxt('Baraye por kardane kamion ~g~E~s~ ra feshar dahid', 2, 1, 0.5, 0.8, 0.6, 255, 255, 255, 255)
				if (IsControlJustReleased(1, 38)) then
					TriggerServerEvent('moneytransfer:checkjob')
				end
			else
				drawTxt('Baraye gereftane ~b~ Kamion ~s~dokme ~g~E~s~ ra feshar dahid.', 2, 1, 0.5, 0.8, 0.6, 255, 255, 255, 255)
				if (IsControlJustReleased(1, 38)) then
					TriggerServerEvent('moneytransfer:checkjob')
				end
			end	
		else
			drawTxt('Baraye gereftane ~b~ Kamion ~s~dokme ~g~E~s~ ra feshar dahid.', 2, 1, 0.5, 0.8, 0.6, 255, 255, 255, 255)
			if (IsControlJustReleased(1, 38)) then
				TriggerServerEvent('moneytransfer:checkjob')
			end
		end
	else
		drawTxt('Baraye cancel kardane kar dokme ~g~H~s~ ra feshar dahid,', 2, 1, 0.5, 0.8, 0.6, 255, 255, 255, 255)
		if (IsControlJustReleased(1, 74)) then
			TriggerServerEvent('bank:withdrawAmende', destination[l].money)
			onjob = false
			RemoveBlip(deliveryblip)
			SetWaypointOff()
			visits = 1
		end
	end
end

function IsInVehicle()
 local ply = GetPlayerPed(-1)
 if IsPedSittingInAnyVehicle(ply) then
 return true
 else
 return false
 end
end

function startjob()


	TriggerEvent("mt:missiontext", "Be ~g~Maghsad~w~ moshakhas shode beranid.", 10000)
	onjob = true
	l = math.random(1,15) 
	deliveryblip = (AddBlipForCoord(destination[l].x,destination[l].y,destination[l].z))
	SetBlipSprite(deliveryblip, 280)
	SetNewWaypoint(destination[l].x,destination[l].y)
end

function SpawnVan()
	if (IsInVehicle()) then
		if IsVehicleModel(GetVehiclePedIsIn(GetPlayerPed(-1), true), GetHashKey("stockade")) then
			startjob()
		end
	else
	Citizen.Wait(1)
	local myPed = GetPlayerPed(-1)
	local player = PlayerId()
	local vehicle = GetHashKey('stockade')

	RequestModel(vehicle)

	while not HasModelLoaded(vehicle) do
		Wait(1)
	end

	local plate = math.random(100, 900)
	local coords = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0, 5.0, 0)
	local spawned_car = CreateVehicle(vehicle, coords, 180, true, false)
	SetVehicleOnGroundProperly(spawned_car)
	SetVehicleNumberPlateText(spawned_car, "MONEY"..plate)
	SetPedIntoVehicle(myPed, spawned_car, - 1)
	SetModelAsNoLongerNeeded(vehicle)
	Citizen.InvokeNative(0xB736A491E64A32CF, Citizen.PointerValueIntInitialized(spawned_car))
	startjob()
	end
end

function deliverysucces()
	TriggerServerEvent('esx-salary:modify', GetPlayerServerId(PlayerId()), "add", destination[l].money)
	if visits == 10 then --change 3 to however many runs you want a person to be able to make before having to return to the depot
		RemoveBlip(deliveryblip)
		onjob = false
		visits = 1
		TriggerEvent("mt:missiontext", "Baraye daryafte bar be ~g~Khazane~w~ beravid.", 10000)
	else
		RemoveBlip(deliveryblip)
		startjob()
		visits = visits + 1					
	end
end


-- not very clean but i took it from my blip script in wich there are more
--Did not test this part in this script, so if it somehow does not work delete the part below and make your own blip :)
local blips = {
	{title="Money Transfer", colour=4, id=375, x=42.79, y=-655.95, z=31.63},
}

Citizen.CreateThread(function()
    for _, info in pairs(blips) do
      info.blip = AddBlipForCoord(info.x, info.y, info.z)
      SetBlipSprite(info.blip, info.id)
      SetBlipDisplay(info.blip, 4)
      SetBlipScale(info.blip, 0.9)
      SetBlipColour(info.blip, info.colour)
      SetBlipAsShortRange(info.blip, true)
	  BeginTextCommandSetBlipName("STRING")
      AddTextComponentString(info.title)
      EndTextCommandSetBlipName(info.blip)
    end
end)