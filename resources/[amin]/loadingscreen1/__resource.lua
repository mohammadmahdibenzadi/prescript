description 'LoadingScreen!'

files {
    'index.html',
    'full.mp4',
    'loop.mp4',
    'loop-old.mp4',
    'song.ogg',
}

loadscreen 'index.html'
