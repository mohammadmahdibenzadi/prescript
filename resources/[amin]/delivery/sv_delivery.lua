ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

RegisterServerEvent('delivery:checkjob')
AddEventHandler('delivery:checkjob', function()
  local src = source
  local xPlayer = ESX.getPlayerFromId(src)
  local job = xPlayer.job.name
  if job == "postal" or job == "offpolice" then
    TriggerClientEvent('yesdelivery', src)
  else
    TriggerClientEvent('nodelivery', src)
  end
  
end)

RegisterServerEvent('delivery:success')
AddEventHandler('delivery:success', function(price)
  local src = source  
  local xPlayer = ESX.getPlayerFromId(src)
  
  xPlayer.addMoney(price*2)
end)
