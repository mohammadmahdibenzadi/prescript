resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

description 'ESX_CustomThingy'

version '1.0.2'

client_scripts {
  '@essentialmode/locale.lua',
  'locales/en.lua',
  'locales/fr.lua',
  'locales/fa.lua',
  'locales/sv.lua',
  'locales/pl.lua',
  'config.lua',
  'client/main.lua',
  'handsup.lua'
}

server_scripts {
	'@async/async.lua',
	'@mysql-async/lib/MySQL.lua',
  '@essentialmode/locale.lua',
  'locales/en.lua',
  'locales/fr.lua',
  'locales/fa.lua',
  'locales/sv.lua',
  'locales/pl.lua',
  'config.lua',
  'server/main.lua'
}
