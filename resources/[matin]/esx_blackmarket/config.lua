Config              = {}
Config.DrawDistance = 100
Config.Size         = {x = 1.5, y = 1.5, z = 1.5}
Config.Color        = {r = 0, g = 128, b = 255}
Config.Type         = 27
Config.Locale       = 'en'

Config.Zones = {

	blackmarket = {
		Items = {},
		Pos = {
			{x = -540.51,   y = -1637.76,  z = 18.89},   
		}
	},

	craftdrill = {
		Items = {},
		Pos = {
			{x = 121.16,   y = -3191.0,  z = 5.0},   
		}
	},

	craftblowtorch = {
		Items = {},
		Pos = {
			{x = 471.94,   y = -1309.77,  z = 28.23},   
		}
	}

}
